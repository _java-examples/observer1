package org.tastefuljava.examples.observer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ObserverList {
    private static final Logger LOG
            = Logger.getLogger(ObserverList.class.getName());

    private final List<Object> observers = new ArrayList<>();

    public void addObserver(Object observer) {
        observers.add(observer);
    }

    public void removeObserver(Object observer) {
        observers.remove(observer);
    }

    public <T> T getNotifier(Class<T> intf) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Object proxy = Proxy.newProxyInstance(loader, new Class<?>[] {intf},
                (pxy, method, args) -> {
            return dispatch(intf, method, args);
        });
        return intf.cast(proxy);
    }

    private <T> Object dispatch(Class<T> intf, Method method, Object[] args) {
        Object result = null;
        for (Object observer: observers) {
            if (intf.isInstance(observer)) {
                try {
                    result = method.invoke(observer, args);
                } catch (IllegalAccessException
                        | InvocationTargetException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }
}
