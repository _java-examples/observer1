package org.tastefuljava.examples.observer;

public interface PictureObserver {
    public void pictureAdded(Picture pic, Folder folder);
    public void pictureRemoved(Picture pic, Folder folder);
    public void pictureUpdated(Picture oldPic, Picture newPic);
}
