package org.tastefuljava.examples.observer;

public class Storage {
    // ...
    private final ObserverList observers = new ObserverList();
    private final PictureObserver pictureNotifier
            = observers.getNotifier(PictureObserver.class);
    private final FolderObserver folderNotifier
            = observers.getNotifier(FolderObserver.class);
    // ...

    public void addPictureObserver(PictureObserver obs) {
        observers.addObserver(obs);
    }

    public void removePictureObserver(PictureObserver obs) {
        observers.removeObserver(obs);
    }

    public void addFolderObserver(FolderObserver obs) {
        observers.addObserver(obs);
    }

    public void removeFolderObserver(FolderObserver obs) {
        observers.removeObserver(obs);
    }

    // ...

    public void addPicture(Picture pic, Folder folder) {
        // ...
        pictureNotifier.pictureAdded(pic, folder);
    }

    public void removePicture(Picture pic, Folder folder) {
        // ...
        pictureNotifier.pictureRemoved(pic, folder);
    }

    public void updatePicture(Picture pic) {
        Picture old = getPictureById(pic.getId());
        // ...
        pictureNotifier.pictureUpdated(old, pic);
    }

    // ...

    private Picture getPictureById(int id) {
        // ...
        return null;
    }
}
