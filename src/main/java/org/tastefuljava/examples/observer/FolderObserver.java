package org.tastefuljava.examples.observer;

public interface FolderObserver {
    public void folderCreated(Folder folder);
    public void folderDeleted(Folder folder);
    public void folderUpdated(Folder oldFolder, Folder newFolder);
}
